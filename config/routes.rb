Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :listings, only: [:index, :show] do
    post 'comment', to: 'listings#add_comment'
    put 'bookmark', to: 'listings#add_bookmark'
    delete 'bookmark', to: 'listings#delete_bookmark'
  end

  root 'listings#index'
end
