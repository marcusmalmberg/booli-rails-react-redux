class CreateListings < ActiveRecord::Migration[5.0]
  def change
    create_table :listings do |t|
      t.integer :booli_id
      t.integer :list_price
      t.string :street_address
      t.integer :rooms
      t.integer :living_area
      t.integer :rent
      t.integer :floor

      t.timestamps
    end
  end
end
