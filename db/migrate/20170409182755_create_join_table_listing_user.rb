class CreateJoinTableListingUser < ActiveRecord::Migration[5.0]
  def change
    create_join_table :listings, :users do |t|
      t.index [:listing_id, :user_id], unique: true
    end
  end
end
