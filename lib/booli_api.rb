require 'net/http'
require 'json'

class BooliApi
  def self.fetch_listings
    listings = get_listings()

    listings['listings'].each do |q|
      begin
        attributes = {
          booli_id: q['booliId'],
          list_price: q['listPrice'],
          street_address: q['location']['address']['streetAddress'],
          rooms: q['rooms'],
          living_area: q['livingArea'],
          rent: q['rent'],
          floor: q['floor'],
        }

        Listing.create_or_update_by(attributes)
      rescue
        # TODO: Handle error
        puts "Oops. Something went wrong"
      end
    end
    nil
  end

private

  def self.get_listings()
    uri = URI("https://api.booli.se/listings?q=nacka&limit=10&offset=0&#{auth_query}")
    request = Net::HTTP::Get.new(uri)

    response = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == 'https', 'Content-Type' => 'application/json') do |http|
      http.request(request)
    end

    if response.code == "200"
      JSON.parse(response.body)
    else
      { 'listings' => [] }
    end
  end

  def self.auth_query
    caller_id = ENV["BOOLI_API_USER_ID"]
    private_key = ENV["BOOLI_API_KEY"]
    time = Time.now.to_i.to_s
    unique = "%.16x"%rand(9**20).to_s
    hash = Digest::SHA1.hexdigest(caller_id + time + private_key + unique)

    "callerId=#{caller_id}&time=#{time}&unique=#{unique}&hash=#{hash}"
  end

end
