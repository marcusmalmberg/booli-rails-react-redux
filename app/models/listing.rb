class Listing < ApplicationRecord
  has_many :comments
  has_and_belongs_to_many :users

  def self.create_or_update_by(attributes)
    listing = self.find_or_create_by(booli_id: attributes[:booli_id])
    listing.update(attributes)
    listing
  end

  def add_bookmark_for_user(user)
    unless user.listings.include? self
      user.listings << self
    end
  end

  def delete_bookmark_for_user(user)
    user.listings.delete(self)
  end

  def thumbnail
    "https://api.bcdn.se/cache/primary_#{self.booli_id}_140x94.jpg"
  end
end
