class CommentSerializer < ActiveModel::Serializer
  attributes :id, :author, :listing_id, :comment

  def author
    object.user.try(:name)
  end
end
