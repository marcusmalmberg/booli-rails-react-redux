class ListingSerializer < ActiveModel::Serializer
  attributes :id, :street_address, :list_price, :rooms, :living_area, :rent, :floor, :thumbnail
end
