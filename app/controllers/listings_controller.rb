class ListingsController < ApplicationController

  before_action :set_listing, only: [:show, :comment, :add_bookmark, :delete_bookmark]

  def index
    @listings = Listing.order(id: :desc).first(10).map do |listing|
      ::ListingSerializer.new(listing)
    end
    @bookmarks = current_user.listings.map(&:id)
  end

  def show
    @comments = @listing.comments.order(id: :desc).map do |comment|
      ::CommentSerializer.new(comment)
    end
    @bookmarks = current_user.listings.map(&:id)
    @listing = ::ListingSerializer.new(@listing)
  end

  def add_comment
    comment_params = params.require(:comment).permit(:listing_id, :comment)
    comment = Comment.create(comment_params.merge(user_id: current_user.id))
    render json: comment
  end

  def add_bookmark
    @listing.add_bookmark_for_user(current_user)
    render json: current_user.listings.map(&:id)
  end

  def delete_bookmark
    @listing.delete_bookmark_for_user(current_user)
    render json: current_user.listings.map(&:id)
  end

private

  def set_listing
    @listing = Listing.find_by_id(params[:listing_id] || params[:id])
  end
end
