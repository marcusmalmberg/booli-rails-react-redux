# Booli app (Rails+React+Redux)

### Challenge:

Build a webapp that lists objects for sale in Stockholm. You should be able to bookmark and comment on Listings. All listings should be fetched via the Booli API. Time limit: 5 hours.

Online demo: https://booli-rails-react-redux.herokuapp.com/

---

### Key parts:

- Ruby on Rails 5
- React
- Redux
- ES2015+
- Webpack

---

Given the time constraints I took a few decisions:

- Instead of setting up a proper API (with e.g. Grape) I used the normal controllers/routes with ActiveModelSerializer to respond to the requests made then frontend.
- Even though creating tests are quick and easy to do, it still takes time. Since this isn't something that will be used, I decided that we could skip this part (and implement it if there was time spare)
- The UI styling is nonexistent. I just added a few classes and stylesheets as a proof of concept.
- Skipped model validation and error handling
- No user management or authentication. Instead I've created a default user that is always used as the current user

Known problems:

- There's an N+1 problem when fetching and serializing comments. In the current state this could easily be solved with `.includes(:user)`
- `Listing.add_bookmark_for_user` is vulnerable to race condition

Misc:

- Fetching data from Booli API is done manually from the console (using `lib/booli_api.rb`)

---

### Todo:

- React
    - Proptypes
    - Testing with Jest/Enzyme
    - Showcase components with React Storybook
    - UI Design

- Rails
    - Specs
    - Model validation
    - Error handling
    - Fetch new data from Booli API automatically
    - Caching

---

### Setup

Prerequisites:

- Ruby 2.3.3
- Node/NPM/Yarn
- Foreman
- PostgreSQL

Instructions:

1. `bundle install`
2. `yarn`
3. `rake db:setup`
4. Fill in `env.example` and rename to `.env`
5. Open the console `rails c` and run:
    1. `load 'booli_api.rb'`
    2. `BooliApi.fetch_listings`
6. Start the server with `foreman start -f Procfile.dev`
