import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk'

import rootReducer from './reducers';

const configureStore = (railsProps) => {
  const store = createStore(
    rootReducer,
    railsProps,
    composeWithDevTools(
      applyMiddleware(
        thunkMiddleware,
      ),
    )
  )

  return store
}

export default configureStore;
