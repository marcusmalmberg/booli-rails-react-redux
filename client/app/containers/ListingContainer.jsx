import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'

import * as bookmarksActions from '../actions/bookmarks';
import * as commentsActions from '../actions/comments';
import ListingComponent from '../components/ListingComponent';
import CommentBoxComponent from '../components/CommentBoxComponent';

export const Listing = (props) => {
  const {
    bookmarks,
    comments,
    listing,

    submitComment,
    toggleBookmark,
  } = props

  const isBookmarked = (listing) => {
    return bookmarks.indexOf(listing.id) !== -1;
  }

  return (
    <div>
      <ListingComponent listing={listing} bookmarked={isBookmarked(listing)} toggleBookmark={toggleBookmark} />
      <CommentBoxComponent comments={comments} listingId={listing.id} submitComment={submitComment}/>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    bookmarks: state.bookmarks,
    comments: state.comments,
    listing: state.listing,
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    ...bookmarksActions,
    ...commentsActions,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Listing);
