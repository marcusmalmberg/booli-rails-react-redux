import React from 'react';
import { connect } from 'react-redux';

import ListingsItemComponent from '../components/ListingsItemComponent';

export const ListingsContainer = (props) => {
  const {
    bookmarks,
    listings,
  } = props

  const isBookmarked = (listing) => {
    return bookmarks.indexOf(listing.id) !== -1;
  }

  return (
    <div className="box">
      {listings.map((listing) => {
        return <ListingsItemComponent key={listing.id} listing={listing} bookmarked={isBookmarked(listing)}/>
      })}
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    bookmarks: state.bookmarks,
    listings: state.listings,
  };
}

export default connect(mapStateToProps)(ListingsContainer);
