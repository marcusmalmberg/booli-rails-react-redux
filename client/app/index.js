import ReactOnRails from 'react-on-rails';

import Listing from './mounts/Listing';
import Listings from './mounts/Listings';

ReactOnRails.register({
  Listing,
  Listings,
});
