import React from 'react';
import { Provider } from 'react-redux';

import configureStore from '../store';
import Listing from '../containers/ListingContainer';

const ListingRoot = (props, _railsContext) => (
  <Provider store={configureStore(props)}>
    <Listing />
  </Provider>
);

export default ListingRoot;
