import React from 'react';
import { Provider } from 'react-redux';

import configureStore from '../store';
import Listings from '../containers/ListingsContainer';

const ListingsRoot = (props, _railsContext) => (
  <Provider store={configureStore(props)}>
    <Listings />
  </Provider>
);

export default ListingsRoot;
