import React from 'react';
import { connect } from 'react-redux';

const ListingComponent = (props) => {
  const {
    listing,
    bookmarked,

    toggleBookmark,
  } = props

  return (
    <div className="box">
      <h4>{listing.street_address}</h4>

      { !bookmarked && <div onClick={() => toggleBookmark(listing.id, true)}>Add bookmark</div> }
      { bookmarked && <div onClick={() => toggleBookmark(listing.id, false)}>Remove bookmark</div> }

      <table>
        <tbody>
          <tr>
            <td>Living area</td>
            <td>{listing.living_area}</td>
          </tr>
          <tr>
            <td>List price</td>
            <td>{listing.list_price}</td>
          </tr>
          <tr>
            <td>Rent</td>
            <td>{listing.rent}</td>
          </tr>
          <tr>
            <td>Rooms</td>
            <td>{listing.rooms}</td>
          </tr>
          <tr>
            <td>Floor</td>
            <td>{listing.floor}</td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default ListingComponent
