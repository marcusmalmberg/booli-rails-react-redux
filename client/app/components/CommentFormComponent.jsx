import React from 'react';

import './CommentFormComponent.scss';

class CommentFormComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      commentText: '',
    }
  }

  handleChange(e) {
    this.setState({commentText: e.target.value});
  }

  handleSubmit(e) {
    e.preventDefault();

    const {
      listingId,
      onSubmit,
    } = this.props

    onSubmit(listingId, this.state.commentText);

    this.setState({commentText: ''})
  }

  render() {
    return (
      <div className="CommentFormComponent">
        <form onSubmit={(e) => this.handleSubmit(e)}>
          Comment: <br />
          <textarea value={this.state.commentText} onChange={(e) => this.handleChange(e)} />
          <br />
          <input type="submit" value="Submit" />
        </form>
      </div>
    );
  }
}

export default CommentFormComponent
