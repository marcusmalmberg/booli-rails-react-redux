import React from 'react';

import './ListingsItemComponent.scss';

const ListingsItemComponent = (props) => {
  const {
    id,
    living_area,
    street_address,
    list_price,
    thumbnail,
  } = props.listing;

  return (
    <a className="ListingsItemComponent"href={`/listings/${id}`}>
      <img src={thumbnail} alt={street_address} className="ListingsItemComponent__thumbnail"/>
      <div className="ListingsItemComponent__details">
        <span className="ListingsItemComponent__details--bold">{street_address}</span><br />
        {living_area} kvm<br />
        {list_price} kr
      </div>
      { props.bookmarked && <span className="ListingsItemComponent__bookmark"><i className="fa fa-star"></i></span>}
    </a>
  )
}

export default ListingsItemComponent
