import React from 'react';

import CommentFormComponent from './CommentFormComponent';
import CommentComponent from './CommentComponent';

const CommentBoxComponent = (props) => {
  const {
    comments,
    listingId,

    submitComment,
  } = props;

  return (
    <div className="box">
      <CommentFormComponent onSubmit={submitComment} listingId={listingId}/>
      {comments.map((comment) => {
        return (
          <CommentComponent
            key={comment.id}
            comment={comment.comment}
            author={comment.author}
          />
        )
      })}
    </div>
  )
}

export default CommentBoxComponent
