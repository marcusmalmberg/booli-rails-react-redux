import React from 'react';

import './CommentComponent.scss';

const CommentComponent = (props) => {
  const {
    id,
    author,
    comment,
  } = props;

  return (
    <div className="CommentComponent">
      {author} wrote:<br />
      {comment}
    </div>
  )
}

export default CommentComponent
