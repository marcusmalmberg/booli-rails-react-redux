import { combineReducers } from 'redux';

import bookmarks from './bookmarks'
import comments from './comments'
import listing from './listing'
import listings from './listings'

const rootReducer = combineReducers({
  bookmarks,
  comments,
  listing,
  listings,
});

export default rootReducer;
