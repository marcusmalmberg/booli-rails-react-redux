import * as actionTypes from '../constants/bookmarks';

const bookmarks = (state = [], action) => {
  const { type } = action;

  switch (type) {
    case actionTypes.RECEIVE_BOOKMARKS: {
      return [...action.bookmarks]
    }
    default: {
      return state;
    }
  }
}

export default bookmarks;
