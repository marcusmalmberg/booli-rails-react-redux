const listings = (state = [], action) => {
  const { type } = action;

  switch (type) {
    default: {
      return state;
    }
  }
}

export default listings;
