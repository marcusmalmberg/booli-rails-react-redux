import * as actionTypes from '../constants/comments';

const comments = (state = [], action) => {
  const { type } = action;

  switch (type) {
    case actionTypes.RECEIVE_COMMENT: {
      return [action.comment, ...state];
    }
    default: {
      return state;
    }
  }
}

export default comments;
