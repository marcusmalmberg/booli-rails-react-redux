import * as actionTypes from '../constants/comments';

export const submitComment = (listingId, comment) => {
  return (dispatch) => {
    return dispatch(submitCommentRequest(listingId, comment));
  }
}

const submitCommentRequest = (listingId, comment) => {
  return (dispatch) => {
    const post_data = {comment: { listing_id: listingId, comment }}
    return $.ajax(`/listings/${listingId}/comment`, { method: 'POST', data: post_data })
      .then((json) => dispatch(receiveComment(json)))
  }
}

const receiveComment = (json) => {
  return {
    type: actionTypes.RECEIVE_COMMENT,
    comment: json,
  }
}
