import * as actionTypes from '../constants/bookmarks';

export const toggleBookmark = (listingId, bookmark) => {
  return (dispatch) => {
    const method = bookmark === true ? 'PUT' : 'DELETE';
    return dispatch(toggleBookmarkRequest(listingId, method));
  }
}

const toggleBookmarkRequest = (listingId, method) => {
  return (dispatch) => {
    return $.ajax(`/listings/${listingId}/bookmark`, { method: method })
      .then((json) => dispatch(receiveBookmarks(json)))
  }
}

const receiveBookmarks = (json) => {
  return {
    type: actionTypes.RECEIVE_BOOKMARKS,
    bookmarks: json,
  }
}
